import BooksNetwork from "@bookstore/data/network/models/booksNetwork";
import { Book } from "@bookstore/domain/models";

export const mapToBook = (book: BooksNetwork.Book): Book => ({
  id: book.id,
  author: book.author,
  title: book.title,
  description: book.description,
});
