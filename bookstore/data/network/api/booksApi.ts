import { inject, injectable } from "inversify";
import { from, map, Observable } from "rxjs";
import CORE_BINDINGS from "@core/di/core.bindings";
import HttpService from "@core/data/network/services/http.service";
import BooksNetwork from "../models/booksNetwork";
import BOOKS_RESOURCE_URLS from "./booksResourceUrls";
import UpdateBook from "@bookstore/domain/models/updateBook.model";
import CreateBook from "@bookstore/domain/models/createBook.model";

@injectable()
class BooksApi {
  @inject(CORE_BINDINGS.HttpService) private httpService!: HttpService;

  getBooks = (): Observable<BooksNetwork.GetResponse> =>
    from(this.httpService.getClient().get<BooksNetwork.GetResponse>(BOOKS_RESOURCE_URLS.CRUD.BOOKS)).pipe(
      map((response) => response.data)
    );

  createBook = (book: CreateBook): Observable<BooksNetwork.PostResponse> =>
    from(this.httpService.getClient().post<BooksNetwork.PostResponse>(BOOKS_RESOURCE_URLS.CRUD.BOOKS, book)).pipe(
      map((response) => response.data)
    );

  updateBook = ({ id, ...rest }: UpdateBook): Observable<BooksNetwork.PostResponse> =>
    from(
      this.httpService.getClient().put<BooksNetwork.PostResponse>(`${BOOKS_RESOURCE_URLS.CRUD.BOOKS}/${id}`, rest)
    ).pipe(map((response) => response.data));

  deleteBook = (id: string): Observable<BooksNetwork.DeleteResponse> =>
    from(
      this.httpService.getClient().delete<BooksNetwork.DeleteResponse>(`${BOOKS_RESOURCE_URLS.CRUD.BOOKS}/${id}`)
    ).pipe(map((response) => response.data));
}

export default BooksApi;
