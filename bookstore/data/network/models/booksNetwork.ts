namespace BooksNetwork {
  export interface GetResponse {
    statusCode: number;
    data: Data;
  }

  export interface PostResponse {
    statusCode: number;
    data: Book;
  }

  export interface PutResponse {
    statusCode: number;
    data: Book;
  }

  export interface DeleteResponse {
    statusCode: number;
    data: string;
  }

  export interface Data {
    books: Book[];
    total: string;
  }

  export interface Book {
    id: string;
    author: string;
    title: string;
    description: string;
  }
}

export default BooksNetwork;
