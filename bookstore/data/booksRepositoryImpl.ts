import { inject, injectable } from "inversify";
import BooksRepository from "@bookstore/domain/repositories/booksRepository";
import BOOK_BINDINGS from "@bookstore/di/book.bindings";
import CreateBook from "@bookstore/domain/models/createBook.model";
import UpdateBook from "@bookstore/domain/models/updateBook.model";

@injectable()
class BooksRepositoryImpl implements BooksRepository {
  @inject(BOOK_BINDINGS.RemoteBooksRepository) private remoteBooksRepository!: BooksRepository;

  getBooks = () => this.remoteBooksRepository.getBooks();
  createBook = (book: CreateBook) => this.remoteBooksRepository.createBook(book);
  updateBook = (book: UpdateBook) => this.remoteBooksRepository.updateBook(book);
  deleteBook = (id: string) => this.remoteBooksRepository.deleteBook(id);
}

export default BooksRepositoryImpl;
