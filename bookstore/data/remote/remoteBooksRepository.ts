import BooksRepository from "@bookstore/domain/repositories/booksRepository";
import { inject, injectable } from "inversify";
import BOOK_BINDINGS from "@bookstore/di/book.bindings";
import BooksApi from "../network/api/booksApi";
import { map } from "rxjs";
import { mapToBook } from "../mappers/booksMapper";
import CreateBook from "@bookstore/domain/models/createBook.model";
import UpdateBook from "@bookstore/domain/models/updateBook.model";

@injectable()
class RemoteBooksRepository implements BooksRepository {
  @inject(BOOK_BINDINGS.BooksApi) private booksApi!: BooksApi;

  getBooks = () => this.booksApi.getBooks().pipe(map((response) => response.data.books.map((book) => mapToBook(book))));
  createBook = (book: CreateBook) => this.booksApi.createBook(book).pipe(map((response) => mapToBook(response.data)));
  updateBook = (book: UpdateBook) => this.booksApi.updateBook(book).pipe(map((response) => mapToBook(response.data)));
  deleteBook = (id: string) => this.booksApi.deleteBook(id).pipe(map((response) => response.data));
}

export default RemoteBooksRepository;
