import { Container } from "inversify";
import coreContainer from "@core/di/inversify.config";
import BOOK_BINDINGS from "./book.bindings";
import BooksApi from "@bookstore/data/network/api/booksApi";
import BooksRepository from "@bookstore/domain/repositories/booksRepository";
import BooksRepositoryImpl from "@bookstore/data/booksRepositoryImpl";
import RemoteBooksRepository from "@bookstore/data/remote/remoteBooksRepository";
import GetBooksUseCase from "@bookstore/domain/usecases/getBooksUseCase";
import CreateBookUseCase from "@bookstore/domain/usecases/createBookUseCase";
import UpdateBookUseCase from "@bookstore/domain/usecases/updateBookUseCase";
import DeleteBookUseCase from "@bookstore/domain/usecases/deleteBookUseCase";

/**
 * Initialize empty DI container to store Books Module bindings
 */
const newContainer = new Container();

/**
 * Create DI container for Books Module and merge Core Module DI Container to it, in order to access its implementations
 */
const booksContainer = Container.merge(coreContainer, newContainer);

/**
 * Bind API to Books Module DI Container
 */
booksContainer.bind<BooksApi>(BOOK_BINDINGS.BooksApi).to(BooksApi);

/**
 * Bind Repository Pattern implementations to Books Module DI Container
 */
booksContainer.bind<BooksRepository>(BOOK_BINDINGS.BooksRepository).to(BooksRepositoryImpl);
booksContainer.bind<BooksRepository>(BOOK_BINDINGS.RemoteBooksRepository).to(RemoteBooksRepository);

/**
 * Bind Use Cases to Books Module DI Container
 */
booksContainer.bind<GetBooksUseCase>(BOOK_BINDINGS.GetBooksUseCase).to(GetBooksUseCase);
booksContainer.bind<CreateBookUseCase>(BOOK_BINDINGS.CreateBookUseCase).to(CreateBookUseCase);
booksContainer.bind<UpdateBookUseCase>(BOOK_BINDINGS.UpdateBookUseCase).to(UpdateBookUseCase);
booksContainer.bind<DeleteBookUseCase>(BOOK_BINDINGS.DeleteBookUseCase).to(DeleteBookUseCase);

export default booksContainer;
