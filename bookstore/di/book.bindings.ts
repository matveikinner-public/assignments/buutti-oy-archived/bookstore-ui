export default {
  BooksApi: Symbol.for("BooksApi"),
  BooksRepository: Symbol.for("BooksRepository"),
  RemoteBooksRepository: Symbol.for("RemoteBooksRepository"),
  GetBooksUseCase: Symbol.for("GetBooksUseCase"),
  CreateBookUseCase: Symbol.for("CreateBookUseCase"),
  UpdateBookUseCase: Symbol.for("UpdateBookUseCase"),
  DeleteBookUseCase: Symbol.for("DeleteBookUseCase"),
};
