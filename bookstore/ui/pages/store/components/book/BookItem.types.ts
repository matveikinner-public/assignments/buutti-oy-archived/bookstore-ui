import { Book } from "@bookstore/domain/models";

interface BookProps {
  book: Book;
}

export default BookProps;
