import React, { FunctionComponent, useState } from "react";
import { useDispatch } from "react-redux";
import { ListItem, ListItemText, Typography, Paper, Collapse, Box, TextField, Button } from "@mui/material";
import { LiteralUnion, RegisterOptions, useForm } from "react-hook-form";
import * as R from "ramda";
import BookProps from "./BookItem.types";
import { Book } from "@bookstore/domain/models";
import {
  createBookRequest,
  deleteBookRequest,
  updateBookRequest,
} from "@bookstore/ui/adapters/redux/books/books.actions";

const BookItem: FunctionComponent<BookProps> = ({ book }: BookProps) => {
  const { id, author, title, description } = book;
  const [truncate, setTruncate] = useState(false);
  const [isReadMore, setIsReadMore] = useState(true);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Omit<Book, "id">>();
  const dispatch = useDispatch();

  const handleEllipsis = (str: string) => {
    if (str.length < 175) return str;
    return isReadMore ? str.slice(0, 150) : str;
  };

  const handleReadMore = (str: string) => {
    if (str.length < 175) return "";
    return isReadMore ? " ...read more" : " show less";
  };

  const handleCreate = (data: Omit<Book, "id">) => dispatch(createBookRequest(data));
  const handleSave = (data: Omit<Book, "id">) => dispatch(updateBookRequest({ id, ...data }));
  const handleDelete = () => dispatch(deleteBookRequest(id));

  const displayError = (errorType: LiteralUnion<keyof RegisterOptions, string>): string => {
    return errorType.toUpperCase();
  };

  return (
    <Paper elevation={1} sx={{ mb: 2 }}>
      <ListItem alignItems='flex-start' onClick={() => setTruncate(!truncate)}>
        <ListItemText
          primary={title}
          secondary={
            <>
              <Typography
                sx={{ display: "inline", cursor: "pointer" }}
                component='span'
                variant='body2'
                color='text.primary'
              >
                {`${author} - `}
              </Typography>
              {handleEllipsis(description)}
              <Typography
                sx={{ display: "inline", cursor: "pointer" }}
                component='span'
                variant='body2'
                color='text.primary'
                onClick={(e: React.MouseEvent<HTMLSpanElement>) => {
                  e.stopPropagation();
                  setIsReadMore(!isReadMore);
                }}
              >
                {handleReadMore(description)}
              </Typography>
            </>
          }
        />
      </ListItem>
      <Collapse
        orientation='vertical'
        in={truncate}
        sx={{
          "& .MuiCollapse-wrapperInner": {
            mx: 1,
          },
        }}
      >
        <Box
          component='form'
          sx={{
            my: 1,
            "& > :not(style)": { my: 1 },
          }}
          noValidate
          autoComplete='off'
        >
          <TextField
            fullWidth
            id='title'
            label='Title'
            variant='outlined'
            defaultValue={title}
            {...register("title", { pattern: /^[a-zA-Z0-9-_.\s]+$/i, required: true })}
            helperText={errors.title ? displayError(errors.title.type) : null}
            error={!R.isNil(errors.title)}
          />
          <TextField
            fullWidth
            id='author'
            label='Author'
            variant='outlined'
            defaultValue={author}
            {...register("author", { pattern: /^[a-zA-Z0-9-_.\s]+$/i, required: true })}
            helperText={errors.author ? displayError(errors.author.type) : null}
            error={!R.isNil(errors.author)}
          />
          <TextField
            fullWidth
            id='description'
            label='Description'
            multiline
            maxRows={4}
            variant='outlined'
            defaultValue={description}
            {...register("description", { pattern: /^[a-zA-Z0-9-_.\s]+$/i, required: true })}
            helperText={errors.description ? displayError(errors.description.type) : null}
            error={!R.isNil(errors.description)}
          />
          <Button variant='outlined' sx={{ mr: 1 }} onClick={handleSubmit(handleCreate)}>
            Save New
          </Button>
          <Button variant='outlined' sx={{ mr: 1 }} onClick={handleSubmit(handleSave)}>
            Save
          </Button>
          <Button variant='outlined' onClick={handleDelete}>
            Delete
          </Button>
        </Box>
      </Collapse>
    </Paper>
  );
};

export default BookItem;
