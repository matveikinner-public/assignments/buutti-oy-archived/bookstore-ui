import { put } from "@redux-saga/core/effects";
import { createLoader, removeLoader } from "@core/ui/adapters/redux/loader/loader.actions";
import { CreateBookRequest } from "../../redux/books/books.types";
import booksContainer from "@bookstore/di/books.container";
import CreateBookUseCase from "@bookstore/domain/usecases/createBookUseCase";
import BOOK_BINDINDS from "@bookstore/di/book.bindings";
import { lastValueFrom, Observable } from "rxjs";
import { Book } from "@bookstore/domain/models";
import { createBookFailure, createBookSuccess } from "../../redux/books/books.actions";
import { createErrorToast, createSuccessToast } from "@core/ui/adapters/redux/toast/toast.actions";

export function* createBookRequestSaga({ payload }: CreateBookRequest) {
  yield put(createLoader());
  const createBookUseCase = booksContainer.get<CreateBookUseCase>(BOOK_BINDINDS.CreateBookUseCase);

  try {
    const createdBook = (yield lastValueFrom((yield createBookUseCase.invoke(payload)) as Observable<Book>)) as Book;
    yield put(createBookSuccess(createdBook));
    yield put(createSuccessToast("Success while attempting to create a new book"));
  } catch (err) {
    yield put(createBookFailure());
    yield put(createErrorToast("Error while attempting to create a new book"));
  } finally {
    yield put(removeLoader());
  }
}
