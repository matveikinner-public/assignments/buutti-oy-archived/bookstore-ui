import { put } from "@redux-saga/core/effects";
import { createLoader, removeLoader } from "@core/ui/adapters/redux/loader/loader.actions";
import { UpdateBookRequest } from "../../redux/books/books.types";
import booksContainer from "@bookstore/di/books.container";
import UpdateBookUseCase from "@bookstore/domain/usecases/updateBookUseCase";
import BOOK_BINDINDS from "@bookstore/di/book.bindings";
import { Book } from "@bookstore/domain/models";
import { lastValueFrom, Observable } from "rxjs";
import { updateBookFailure, updateBookSuccess } from "../../redux/books/books.actions";
import { createErrorToast, createSuccessToast } from "@core/ui/adapters/redux/toast/toast.actions";

export function* updateBookRequestSaga({ payload }: UpdateBookRequest) {
  yield put(createLoader());
  const updateBookUseCase = booksContainer.get<UpdateBookUseCase>(BOOK_BINDINDS.UpdateBookUseCase);

  try {
    const updatedBook = (yield lastValueFrom((yield updateBookUseCase.invoke(payload)) as Observable<Book>)) as Book;
    yield put(updateBookSuccess(updatedBook));
    yield put(createSuccessToast("Success while attempting to update an existing books"));
  } catch (err) {
    yield put(updateBookFailure());
    yield put(createErrorToast("Error while attempting to update an existing books"));
  } finally {
    yield put(removeLoader());
  }
}
