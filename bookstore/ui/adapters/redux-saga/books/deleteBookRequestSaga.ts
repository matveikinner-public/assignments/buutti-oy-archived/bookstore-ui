import { put } from "@redux-saga/core/effects";
import { createLoader, removeLoader } from "@core/ui/adapters/redux/loader/loader.actions";
import { DeleteBookRequest } from "../../redux/books/books.types";
import booksContainer from "@bookstore/di/books.container";
import DeleteBookUseCase from "@bookstore/domain/usecases/deleteBookUseCase";
import BOOK_BINDINDS from "@bookstore/di/book.bindings";
import { lastValueFrom, Observable } from "rxjs";
import { createBookFailure, deleteBookSuccess } from "../../redux/books/books.actions";
import { createErrorToast, createSuccessToast } from "@core/ui/adapters/redux/toast/toast.actions";

export function* deleteBookRequestSaga({ payload }: DeleteBookRequest) {
  yield put(createLoader());
  const deleteBookUseCase = booksContainer.get<DeleteBookUseCase>(BOOK_BINDINDS.DeleteBookUseCase);

  try {
    (yield lastValueFrom((yield deleteBookUseCase.invoke(payload)) as Observable<string>)) as string;
    yield put(deleteBookSuccess(payload));
    yield put(createSuccessToast("Success while attempting to delete an existing book"));
  } catch (err) {
    yield put(createBookFailure());
    yield put(createErrorToast("Error while attempting to delete an existing book"));
  } finally {
    yield put(removeLoader());
  }
}
