import { all, takeLatest } from "@redux-saga/core/effects";
import {
  CREATE_BOOK_REQUEST,
  DELETE_BOOK_REQUEST,
  GET_BOOKS_REQUEST,
  UPDATE_BOOK_REQUEST,
} from "../redux/books/books.contants";
import { createBookRequestSaga } from "./books/createBookRequestSaga";
import { deleteBookRequestSaga } from "./books/deleteBookRequestSaga";
import { getBooksRequestSaga } from "./books/getBooksRequestSaga";
import { updateBookRequestSaga } from "./books/updateBookRequestSaga";

export default function* root() {
  yield all([
    takeLatest(GET_BOOKS_REQUEST, getBooksRequestSaga),
    takeLatest(CREATE_BOOK_REQUEST, createBookRequestSaga),
    takeLatest(UPDATE_BOOK_REQUEST, updateBookRequestSaga),
    takeLatest(DELETE_BOOK_REQUEST, deleteBookRequestSaga),
  ]);
}
