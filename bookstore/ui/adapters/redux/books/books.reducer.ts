import { Book } from "@bookstore/domain/models";
import {
  CREATE_BOOK_FAILURE,
  CREATE_BOOK_REQUEST,
  CREATE_BOOK_SUCCESS,
  DELETE_BOOK_FAILURE,
  DELETE_BOOK_REQUEST,
  DELETE_BOOK_SUCCESS,
  GET_BOOKS_FAILURE,
  GET_BOOKS_REQUEST,
  GET_BOOKS_SUCCESS,
  UPDATE_BOOK_FAILURE,
  UPDATE_BOOK_REQUEST,
  UPDATE_BOOK_SUCCESS,
} from "./books.contants";
import { BooksActionTypes } from "./books.types";

const initialState: Book[] = [];

const booksReducer = (state = initialState, action: BooksActionTypes) => {
  switch (action.type) {
    case GET_BOOKS_REQUEST:
      return state;
    case GET_BOOKS_SUCCESS:
      return [...state, ...action.payload].filter((v, i, a) => a.findIndex((t) => t.id === v.id) === i);
    case GET_BOOKS_FAILURE:
      return state;
    case CREATE_BOOK_REQUEST:
      return state;
    case CREATE_BOOK_SUCCESS:
      return [...state, action.payload];
    case CREATE_BOOK_FAILURE:
      return state;
    case UPDATE_BOOK_REQUEST:
      return state;
    case UPDATE_BOOK_SUCCESS:
      return state.filter((book) => book.id !== action.payload.id).concat(action.payload);
    case UPDATE_BOOK_FAILURE:
      return state;
    case DELETE_BOOK_REQUEST:
      return state;
    case DELETE_BOOK_SUCCESS:
      return state.filter((book) => book.id !== action.payload);
    case DELETE_BOOK_FAILURE:
      return state;
    default:
      return state;
  }
};

export default booksReducer;
