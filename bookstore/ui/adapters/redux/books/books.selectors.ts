import { RootState } from "@core/ui/frameworks/redux.config";
import { BooksState } from "./books.types";

export const selectBooks = (state: RootState): BooksState => state.bookstore.books;
