import {
  CREATE_BOOK_FAILURE,
  CREATE_BOOK_REQUEST,
  CREATE_BOOK_SUCCESS,
  DELETE_BOOK_FAILURE,
  DELETE_BOOK_REQUEST,
  DELETE_BOOK_SUCCESS,
  GET_BOOKS_FAILURE,
  GET_BOOKS_REQUEST,
  GET_BOOKS_SUCCESS,
  UPDATE_BOOK_FAILURE,
  UPDATE_BOOK_REQUEST,
  UPDATE_BOOK_SUCCESS,
} from "./books.contants";
import { Book } from "@bookstore/domain/models";
import CreateBook from "@bookstore/domain/models/createBook.model";
import UpdateBook from "@bookstore/domain/models/updateBook.model";

export const getBooksRequest = () => ({
  type: GET_BOOKS_REQUEST,
});

export const getBooksSuccess = (payload: Book[]) => ({
  type: GET_BOOKS_SUCCESS,
  payload,
});

export const getBooksFailure = () => ({
  type: GET_BOOKS_FAILURE,
});

export const createBookRequest = (payload: CreateBook) => ({
  type: CREATE_BOOK_REQUEST,
  payload,
});

export const createBookSuccess = (payload: Book) => ({
  type: CREATE_BOOK_SUCCESS,
  payload,
});

export const createBookFailure = () => ({
  type: CREATE_BOOK_FAILURE,
});

export const updateBookRequest = (payload: UpdateBook) => ({
  type: UPDATE_BOOK_REQUEST,
  payload,
});

export const updateBookSuccess = (payload: Book) => ({
  type: UPDATE_BOOK_SUCCESS,
  payload,
});

export const updateBookFailure = () => ({
  type: UPDATE_BOOK_FAILURE,
});

export const deleteBookRequest = (payload: string) => ({
  type: DELETE_BOOK_REQUEST,
  payload,
});

export const deleteBookSuccess = (payload: string) => ({
  type: DELETE_BOOK_SUCCESS,
  payload,
});

export const deleteBookFailure = () => ({
  type: DELETE_BOOK_FAILURE,
});
