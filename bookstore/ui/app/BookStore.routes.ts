import { RouteProps } from "react-router-dom";
import Store from "../pages/store/Store";

const routes: RouteProps[] = [
  {
    path: "/",
    exact: true,
    component: Store,
  },
];

export default routes;
