import { Observable } from "rxjs";
import { Book } from "../models";
import CreateBook from "../models/createBook.model";
import UpdateBook from "../models/updateBook.model";

interface BooksRepository {
  getBooks(): Observable<Book[]>;
  createBook(book: CreateBook): Observable<Book>;
  updateBook(book: UpdateBook): Observable<Book>;
  deleteBook(id: string): Observable<string>;
}

export default BooksRepository;
