import { AtLeast } from "@core/ui/utils/typeExtensions";
import { Book } from ".";

type UpdateBook = AtLeast<Book, "id">;

export default UpdateBook;
