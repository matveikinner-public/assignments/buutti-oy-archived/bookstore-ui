export type { default as Book } from "./book.model";
export type { default as CreateBook } from "./createBook.model";
export type { default as UpdateBook } from "./updateBook.model";
