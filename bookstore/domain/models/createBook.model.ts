import { Book } from ".";

type CreateBook = Omit<Book, "id">;

export default CreateBook;
