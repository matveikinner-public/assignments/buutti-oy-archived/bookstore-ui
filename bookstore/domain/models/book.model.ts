export default interface Book {
  id: string;
  author: string;
  title: string;
  description: string;
}
