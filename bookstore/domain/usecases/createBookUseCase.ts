import { inject, injectable } from "inversify";
import BOOK_BINDINGS from "@bookstore/di/book.bindings";
import BooksRepository from "../repositories/booksRepository";
import CreateBook from "../models/createBook.model";

@injectable()
class CreateBookUseCase {
  @inject(BOOK_BINDINGS.BooksRepository) private booksRepository!: BooksRepository;

  invoke(book: CreateBook) {
    return this.booksRepository.createBook(book);
  }
}

export default CreateBookUseCase;
