import { injectable, inject } from "inversify";
import BOOK_BINDINGS from "@bookstore/di/book.bindings";
import BooksRepository from "../repositories/booksRepository";

@injectable()
class DeleteBookUseCase {
  @inject(BOOK_BINDINGS.BooksRepository) private booksRepository!: BooksRepository;

  invoke = (id: string) => this.booksRepository.deleteBook(id);
}

export default DeleteBookUseCase;
