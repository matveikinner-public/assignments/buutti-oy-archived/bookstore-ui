import { inject, injectable } from "inversify";
import BOOK_BINDINGS from "@bookstore/di/book.bindings";
import BooksRepository from "../repositories/booksRepository";
import UpdateBook from "../models/updateBook.model";

@injectable()
class UpdateBookUseCase {
  @inject(BOOK_BINDINGS.BooksRepository) private booksRepository!: BooksRepository;

  invoke = (book: UpdateBook) => this.booksRepository.updateBook(book);
}

export default UpdateBookUseCase;
