/**
 * TS helper extension to require at least one property of T
 */
export type AtLeast<T, K extends keyof T> = Partial<T> & Required<Pick<T, K>>;
