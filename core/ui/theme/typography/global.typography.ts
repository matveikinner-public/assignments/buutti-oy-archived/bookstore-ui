import { ThemeOptions } from "@mui/material";

const globalTypography: ThemeOptions["typography"] = {
  fontFamily: "Jost, sans-serif",
};

export default globalTypography;
