import { AlertProps } from "@mui/material";
import { CREATE_ERROR_TOAST, CREATE_SUCCESS_TOAST, RESET_TOAST } from "./toast.constants";

export type ToastType = "success" | "alert" | null;
export interface CreateSuccessToast {
  type: typeof CREATE_SUCCESS_TOAST;
  payload: string;
}

export interface CreateAlertToast {
  type: typeof CREATE_ERROR_TOAST;
  payload: string;
}

export interface ResetToast {
  type: typeof RESET_TOAST;
}

export interface ToastState {
  type: AlertProps["severity"];
  message: string;
}

export type ToastActionTypes = CreateSuccessToast | CreateAlertToast | ResetToast;
