import { RootState } from "../../../frameworks/redux.config";
import { ToastState } from "./toast.types";

// eslint-disable-next-line import/prefer-default-export
export const selectToastState = (state: RootState): ToastState => state.core.toast;
