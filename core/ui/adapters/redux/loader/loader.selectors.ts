import { RootState } from "../../../frameworks/redux.config";
import { LoaderState } from "./loader.types";

export const selectLoader = (state: RootState): LoaderState => state.core.loader;
