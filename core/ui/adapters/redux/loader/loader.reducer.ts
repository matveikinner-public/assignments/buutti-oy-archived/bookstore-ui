import { CREATE_LOADER, REMOVE_LOADER } from "./loader.constants";
import { LoaderActionTypes, LoaderState } from "./loader.types";

const initialState: LoaderState = {
  isActive: false,
  tasks: 0,
};

const loaderReducer = (state = initialState, action: LoaderActionTypes): LoaderState => {
  switch (action.type) {
    case CREATE_LOADER:
      return {
        isActive: true,
        tasks: state.tasks + 1,
      };
    case REMOVE_LOADER:
      return {
        isActive: state.tasks > 1,
        tasks: state.tasks - 1,
      };
    default:
      return state;
  }
};

export default loaderReducer;
