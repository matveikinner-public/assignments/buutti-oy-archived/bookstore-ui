import React, { FunctionComponent } from "react";
import { useSelector } from "react-redux";
import { LinearProgress } from "@mui/material";
import { selectLoader } from "../../adapters/redux/loader/loader.selectors";

const Loader: FunctionComponent = () => {
  const loader = useSelector(selectLoader);

  if (loader.isActive) {
    return (
      <LinearProgress
        sx={{
          position: "absolute",
          top: 0,
          right: 0,
          left: 0,
        }}
      />
    );
  }
  return null;
};

export default Loader;
