import { Container } from "inversify";
import IHttpService from "@core/domain/interfaces/http.interface";
import HttpService from "@core/data/network/services/http.service";
import CORE_BINDINGS from "./core.bindings";

const coreContainer = new Container();

coreContainer.bind<IHttpService>(CORE_BINDINGS.HttpService).to(HttpService);

export default coreContainer;
